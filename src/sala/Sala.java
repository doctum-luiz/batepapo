package sala;

import java.util.ArrayList;
import java.util.Date;

import usuario.Usuario;

public class Sala {
	private int id;
	private String nome;
	private Date data_criado;
	private Usuario usuario_criador;
	private ArrayList<Usuario> usuariosConectados;
	
	
	public int getId() {
		return id;
	}
	
	public String getNome() {
		return nome;
	}
	public Date getData_criado() {
		return data_criado;
	}
	public Usuario getUsuario_criador() {
		return usuario_criador;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setData_criado(Date data_criado) {
		this.data_criado = data_criado;
	}
	public void setUsuario_criador(Usuario usuario_criador) {
		this.usuario_criador = usuario_criador;
	}

	public ArrayList<Usuario> getUsuariosConectados() {
		return usuariosConectados;
	}

	public void setUsuariosConectados(ArrayList<Usuario> usuariosConectados) {
		this.usuariosConectados = usuariosConectados;
	}
	
	public void imprimir() {
		System.out.println("---=="+this.getId()+" : "+this.getNome()+"==---");
		System.out.println("Usuario Criado: "+this.getUsuario_criador().getNome());
		System.out.println("Usuários Conectados:");
		for(Usuario user: this.getUsuariosConectados()) {
			user.imprimir();
		}
	}
}
