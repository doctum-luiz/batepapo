package sala;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import usuario.Usuario;

import org.apache.ibatis.annotations.Result;

public interface SalaMAPPER {

	@Insert("INSERT INTO sala(nome, data_criada, usuario_criador) "
			+ "VALUES (#{nome}, #{data_criada}, #{usuario_criador.id,jdbcType=INTEGER})")
	void insertSala(Sala sala);

	@Results(id = "salaEUsuarios", value = {
			@Result(property = "usuariosConectados", column = "id", javaType = ArrayList.class, 
								many = @Many(select = "usuario.UsuarioMAPPER.selectUsuariosBySala")),
			@Result(property = "id", column = "id", javaType = Integer.class),
			@Result(property = "usuario_criador", column="usuario_criador", javaType=Usuario.class,
					one=@One(select="usuario.UsuarioMAPPER.selectById"))
			}
			
	)
	@Select("SELECT * FROM sala WHERE id = #{sala}")
	Sala selectSala(int sala);

}
