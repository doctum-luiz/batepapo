package sala;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.session.SqlSession;

import conexao.Conexao;
import usuario.Usuario;
import usuario.UsuarioMAPPER;

public class SalaDAO {

	public void save(Sala sala) {
		SqlSession session = Conexao.getSqlSessionFactory().openSession();
		SalaMAPPER mapper = session.getMapper(SalaMAPPER.class);
		mapper.insertSala(sala);
		session.commit();
		session.close();
	}
	
	public Sala selectById(int id) {
		SqlSession session = Conexao.getSqlSessionFactory().openSession();
		SalaMAPPER mapper = session.getMapper(SalaMAPPER.class);
		Sala sala = mapper.selectSala(id);
		session.close();
		return sala;
	}
	
}
