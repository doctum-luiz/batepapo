package usuario;

import java.util.ArrayList;

import org.apache.ibatis.session.SqlSession;
import conexao.Conexao;

public class UsuarioDAO {
	
	public void save(Usuario usuario) {
		SqlSession session = Conexao.getSqlSessionFactory().openSession();
		UsuarioMAPPER mapper = session.getMapper(UsuarioMAPPER.class);
		mapper.insertUsuario(usuario);
		session.commit();
		session.close();
	}
	
	public Usuario selectById(int id) {
		SqlSession session = Conexao.getSqlSessionFactory().openSession();
		UsuarioMAPPER mapper = session.getMapper(UsuarioMAPPER.class);
		Usuario usuario = mapper.selectById(id);
		session.close();
		return usuario;
	}
	
	public ArrayList<Usuario> selectUsuariosBySala(Integer sala) {
		SqlSession session = Conexao.getSqlSessionFactory().openSession();
		UsuarioMAPPER mapper = session.getMapper(UsuarioMAPPER.class);
		ArrayList<Usuario> usuarios = mapper.selectUsuariosBySala(sala);
		session.close();
		return usuarios;
	}

}
