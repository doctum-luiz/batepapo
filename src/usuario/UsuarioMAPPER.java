package usuario;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

public interface UsuarioMAPPER {
	
	@Insert("INSERT INTO usuario(usuario, nome) "
			+ "VALUES (#{usuario}, #{nome})")
	void insertUsuario(Usuario usuario);
	
	@Select("SELECT * FROM usuario WHERE id = #{id}")
	Usuario selectById(int id);
	
	@Results(id="userBySala",value= {
			@Result(property="id", column="id", javaType=Integer.class)})
	@Select("SELECT usuario.* "
			+ "FROM usuario_sala "
			+ "INNER JOIN usuario ON usuario.id = usuario_sala.usuario"
			+ " WHERE usuario_sala.sala = #{sala}"
			+ "   AND usuario_sala.saida is null")
	ArrayList<Usuario> selectUsuariosBySala(Integer sala);
	
}