package usuario;

import java.util.ArrayList;

import sala.Sala;

public class Usuario {
	public Integer id;
	public String usuario;
	public String nome;
	public ArrayList<Sala> salaConectados;
	
	public Usuario() {
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void imprimir() {
		System.out.println("---==Usuario==---");
		System.out.println("Id: "+this.getId());
		System.out.println("Usuario: "+this.getUsuario());
		System.out.println("Nome: "+this.getNome());
		System.out.println("-----------------");
	}
	
}
