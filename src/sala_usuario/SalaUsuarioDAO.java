package sala_usuario;

import org.apache.ibatis.session.SqlSession;

import conexao.Conexao;
import sala.Sala;
import usuario.Usuario;

public class SalaUsuarioDAO {
	
	public void save(Usuario usuario, Sala sala) {
		SqlSession session = Conexao.getSqlSessionFactory().openSession();
		SalaUsuarioMAPPER mapper = session.getMapper(SalaUsuarioMAPPER.class);
		mapper.insertSalaUsuario(usuario, sala);
		session.commit();
		session.close();
	}

}
