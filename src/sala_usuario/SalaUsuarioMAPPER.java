package sala_usuario;

import org.apache.ibatis.annotations.Insert;

import sala.Sala;
import usuario.Usuario;

public interface SalaUsuarioMAPPER {
	
	@Insert("INSERT INTO sala_usuario(usuario, sala) "
			+ "VALUES (#{usuario.id}, #{sala.id})")
	void insertSalaUsuario(Usuario usuario, Sala sala);
	
}
